import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

// Класс выбора действий в проге
public class Chose {
    // Метод с которого начинается прога
    public void start(String[] args) throws IOException {
        System.out.println("args" + Arrays.toString(args));

        Slova words = new Slova();
        Symbols sym = new Symbols();
        String argument = null;
        if (args.length == 0)
            chose();
        else {
            for (int i = 0; i < args.length; i += 2) {
                if (args[i].toString().equals("-word")) {
                    argument = args[i + 1];
                    System.out.println(words.peredacha(argument));
                } else if (args[i].toString().equals("-symbols")) {
                    argument = args[i + 1];
                    sym.readFile(argument);
                }
            }
        }
    }

    public static Scanner ins;

    // Если нет аргументов, программа будет обрабатывать стандартный файл
    public void chose() throws IOException {
        ins = new Scanner(System.in);
        Slova word = new Slova();
        Symbols symbols = new Symbols();
        int i = 0;
        String s = "C:\\textFile.txt";
        while (i != 3) {
            System.out.println("Введите команду:\n 1--Кол-во слов в тексте\n 2--Кол-во символов в тексте \n 3--Выход");
            i = ins.nextInt();
            if (i == 1) {
                try {
                    System.out.println(word.peredacha("C:\\textFile.txt"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (i == 2)

                symbols.readFile("C:\\textFile.txt");
            if (i == 3)
                break;
        }

    }


}

