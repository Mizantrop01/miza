import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Slova {
// ПОДСЧЕТ СЛОВ
    public  String readFile(String argument) throws IOException {
        String str = "";
        try {
            File file = new File(argument);
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            while (bufferedReader.ready()) {
                str += bufferedReader.readLine();
                str += " ";
            }
        } catch (Exception e) {
        }
        return str;
    }

    public HashMap<String, Integer> parsingText(String argument) throws IOException {

        int i = 0, povtor = 0, j = 0;
        String word = null, dWord = null;
        String s = " ";

        s = readFile(argument);
        s = s.toLowerCase();
        List<String> list = new ArrayList();
        List<String> exception = new ArrayList();
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        StringTokenizer st = new StringTokenizer(s, "\t\n\r,.;:\\1234567890-=+-/ ");
        while (st.hasMoreTokens())
            list.add(st.nextToken());

        while (i < list.size()) {
            word = list.get(i);//присваеваем эл-т
            if (exception.contains(word))
                i++;
            else {
                j = i;
                while (j < list.size()) {  //запускаем обход массива, сравнивая каждый элемент с эл-том массива
                    dWord = list.get(j);
                    if (word.equals(dWord)) //если эл-ты массива и word равны, то
                        povtor++; // +1 к счетчику
                    j++;
                    if (list.size() == j) // если строка массива кончилась, то записываем его в коллекцию исключений
                        exception.add(word);
                }

                map.put(word, povtor);
                povtor = 0;
                i++;
            }
        }
        return map;
    }




    public List<Map.Entry<String, Integer>> peredacha(String argument) throws IOException {
        Map<String, Integer> per = parsingText(argument);
        Set<Map.Entry<String, Integer>> set = per.entrySet();
        List<Map.Entry<String, Integer>> orderedList = new ArrayList<Map.Entry<String, Integer>>(set);
        Collections.sort(orderedList, new Comp());
        return orderedList;
    }



    public class Comp implements Comparator<Map.Entry<String, Integer>> {

        @Override
        public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
            Integer val1 = o1.getValue();
            Integer val2 = o2.getValue();
            if (val1 < val2) {
                return 1;
            } else if (val1.equals(val2)) {
                return 0;
            } else {
                return -1;
            }
        }
    }
}

